# Vue 3 + Typescript + Vite + element-plus

## 参考地址

[vite官网](https://vitejs.cn/)
[typescript官网](https://www.tslang.cn/)
[vue3官网](https://v3.cn.vuejs.org/)

## 创建项目

```bash
# npm 6.x
npm init vite@latest vue3-admin --template vue

# npm 7+, 需要额外的双横线：
npm init vite@latest vue3-admin -- --template vue-ts

# yarn
yarn create vite vue3-admin --template vue

# pnpm
pnpm create vite vue3-admin -- --template vue
```

```bash
# 安装最新router
cnpm i vue-router@next --save

# 安装最新vuex
cnpm i vuex@next -S

# 安装axios
cnpm i axios -S

# 安装qs
cnpm i qs -S

# eslint-plugin-vue 文件检查
cnpm i -D -S  eslint eslint-plugin-vue

# 安装typeScript
cnpm i typescript -D
npx tsc --init

# element-plus安装
cnpm install element-plus -S

# 安装normalize.css
npm i normalize.css -S

# fast-glob
npm install fast-glob -S

# svgo是svg 压缩处理优化工具
# 我们很多网上下载或者Sketch导出的 svg 会有很多冗余无用的信息，大大的增加了 svg 的尺寸，我们可以使用svgo对它进行优化。
# 我们在创建src/icons/svgo.yml配置文件。安装svgo，注意需要指定版本号
npm i -D svgo@1.3.2

# 配置vite-plugin-svg-icons

npm install vite-plugin-svg-icons -D

# 自动注册路由
npm install vite-plugin-pages  @types/fs-extra @types/node fs-extra gray-matter -D

# 代理path
npm install path-browserify -S

# 安装path-to-regexp 实现面包屑组件
# 安装固定版本
npm install path-to-regexp@6.2.0 --save

# npm默认安装使用时
# import { compile } from 'path-to-regexp' 
# 老报错compile函数是undefined 时好时坏
```

---

title: tags
date: 2022-03-04 09:39:50
type: tags
layout: tag
---
