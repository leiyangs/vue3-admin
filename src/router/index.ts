import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import Layout from "@/layout/index.vue";
import Record from "@/views/system/Record.vue";
// import children from "pages-generated"; // vite-plugin-pages 生成的路由信息。直接导入会引起类型错误`tsconfig.json`加入 vite-plugin-pages/client

// 最初的路由
export const constantRoutes: Array<RouteRecordRaw> = [
  {
    path: "/",
    component: Layout,
    redirect: "/dashboard",
    children: [
      {
        path: "dashboard",
        name: "dashboard",
        component: () =>
          import(
            /* webpackChunkName: dashboard */ "@/views/dashboard/index.vue"
          ),
        meta: {
          title: "Dashboard",
          // icon: "dashboard",
          icon: "el-icon-eleme-filled", // 菜单支持element icon 以及类型补充
          hidden: false,
        },
      },
    ],
  },
];

//
export const asyncRoutes: Array<RouteRecordRaw> = [
  {
    path: "/documentation",
    component: Layout,
    redirect: "/documentation/index",
    children: [
      {
        path: "index",
        name: "Documentation",
        component: () =>
          import(
            /* webpackChunkName: dashboard */ "@/views/documentation/index.vue"
          ),
        meta: {
          title: "Documentation",
          icon: "documentation",
        },
      },
    ],
  },
  {
    path: "/guide",
    component: Layout,
    redirect: "/guide/index",
    children: [
      {
        path: "index",
        name: "Guide",
        component: () =>
          import(/* webpackChunkName: "guide" */ "@/views/guide/index.vue"),
        meta: {
          title: "Guide",
          icon: "guide",
          activeMenu: "/documentation/index", // guide路由激活时，选中的是documentation菜单
        },
      },
    ],
  },
  {
    path: "/system",
    component: Layout,
    redirect: "/system/user",
    meta: {
      title: "System",
      icon: "lock",
      hidden: false,
      alwaysShow: true, // 默认情况下如果只有一个子路由 就不显示父路由，alwaysShow为true可始终显示父路由
    },
    children: [
      {
        path: "menu",
        component: () =>
          import(/* webpackChunkName: "menu" */ "@/views/system/menu.vue"),
        meta: {
          title: "Menu Management",
          icon: "list",
          hidden: false,
        },
      },
      {
        path: "role",
        component: () =>
          import(/* webpackChunkName: "role" */ "@/views/system/role.vue"),
        meta: {
          title: "Role Management",
          icon: "list",
          hidden: false,
        },
      },
      {
        path: "user",
        component: () =>
          import(/* webpackChunkName: "user" */ "@/views/system/user.vue"),
        meta: {
          title: "User Management",
          icon: "list",
          hidden: false,
        },
      },
      {
        path: "record",
        component: Record,
        redirect: "/system/record/aa",
        meta: {
          title: "Record",
          icon: "list",
          hidden: false,
        },
        children: [
          {
            path: "/system/record",
            redirect: "/system/record/aa",
            component: Record,
            children: [
              {
                path: "aa",
                component: () =>
                  import(/* webpackChunkName: "aa" */ "@/views/system/aa.vue"),
                meta: {
                  title: "AA",
                  icon: "lock",
                },
              },
            ],
          },
        ],
      },
    ],
  },
  {
    //外链路由
    path: "/external-link",
    component: Layout,
    children: [
      {
        path: "https://www.baidu.com",
        redirect: "/",
        meta: {
          title: "External Link",
          icon: "link",
        },
      },
    ],
  },
];

// 自动挂载路由
// export const constantRoutes: Array<RouteRecordRaw> = [
//   {
//     path: "/",
//     component: Layout,
//     redirect: "/dashboard",
//     children,
//   },
// ];

export const routes = [...constantRoutes, ...asyncRoutes];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
