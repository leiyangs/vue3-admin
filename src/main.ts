import { createApp } from "vue";
import App from "./App.vue";
import router from "./router/index";
import store from "./store";
import ElementPlus from "element-plus";
import * as ElementPlusIconsVue from "@element-plus/icons-vue";
// 初始化css 重置css默认样式
import "normalize.css/normalize.css"; // A modern alternative to CSS resets
// 全局css
import "@/styles/index.scss";

import initSvgIcon from "@/icons/index";
import installElementPlus from "./plugins/element"; // 全局挂载element-plus

const app = createApp(App);

// 导入所有element图标并进行全局注册
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}

app.use(ElementPlus);
app.use(router);
app.use(store);
app.use(initSvgIcon);
app.use(installElementPlus);
app.mount("#app");
