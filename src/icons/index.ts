import { App } from "vue";
import SvgIcon from "@/components/SvgIcon/index.vue";

// 使用 require.content 加载./svg目录下所有svg文件
const files = import.meta.globEager<any>("./svg/*.svg");
// 引入注册脚本
import "virtual:svg-icons-register";

export default (app: App) => {
  // 全局注册svg-icon组件
  app.component("svg-icon", SvgIcon);
};
